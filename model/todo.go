package model

type Todo struct {
	Id          string `json:"id"`
	Description string `json:"description"`
	Completed   bool   `json:"completed"`
}

type ByDescription []Todo

func (m ByDescription) Len() int           { return len(m) }
func (m ByDescription) Less(i, j int) bool { return m[i].Description < m[j].Description }
func (m ByDescription) Swap(i, j int)      { m[i], m[j] = m[j], m[i] }

type AddTodoRequest struct {
	Description string `json:"description" validate:"description"`
}

type AddTodoResponse struct {
	Id string `json:"id"`
}