# TODO List Api

Deployment Status | [![Build Status](http://35.246.133.252:8080/buildStatus/icon?job=todo-list-api-deployment)](http://35.246.133.252:8080/view/todolist/job/todo-list-api-deployment/)

Pact Status | [![Pact Status](https://test.pact.dius.com.au/pacts/provider/todolist-api/consumer/todolist-ui/latest/badge.svg)](https://test.pact.dius.com.au/matrix/provider/todolist-api/consumer/todolist-ui)

This project aims to build simple todo list service that will meet the following features.

| Environment  | URL                            |
| :----------: | ------------------------------ |
| Production   | http://35.198.160.161:8000     |

#### Key Features

* Adding a todo to your list
* Removing a todo from your list
* Marking your not completed todo as completed
* Reverting your todo status from completed to not completed
* Clean up your todo list
* List your todos

#### How To Use

To use the project, you will need [Git](https://git-scm.com) and [Go](https://golang.org/) installed on your computer.

* Clone this repository.
    
        git clone https://gitlab.com/seckinsen/todo-list-api.git

* Go into the repository
        
        cd todo-list-api

* Install dependencies

        go mod download

* Execute the following command to booted up the project.

        > execute the project on local environment
            go build && ./todo-list-api
            
        > execute the project on dockerized environment 
            ./build.sh
            
* Then, the application needs to be run on 8000 port.

#### How To Run Unit and CDC Provider Tests

To test the project, you will need [Pact](https://github.com/pact-foundation/pact-ruby-standalone/releases/) standalone executables installed on your computer.

* Execute the following command to pass all the tests.

        go clean -cache && go test ./...

#### How To Deploy

To deploy the project, you will need [Jenkins](https://www.jenkins.io/) which hosted on [K8S](https://cloud.google.com/solutions/jenkins-on-kubernetes-engine-tutorial). 
You can find prepared pipeline file [here](./Jenkinsfile).

#### Design Notes

The project serves REST based API to handle CRUD operations. 

| Features                          | Method    | URL              |
| :-------------------------------  | --------  | ---------------- |
| List your todos                   | GET       | /todos           |
| Adding a todo to your list        | POST      | /todos           |
| Clean up your todo list           | DELETE    | /todos           |
| Removing a todo from your list    | DELETE    | /todos/{id}      |
| Change todo status                | PATCH     | /todos/{id}      |

#### In case of any problem

* Do not hesitate to contact with [Seçkin ŞEN](mailto:sckn.sen@gmail.com)
