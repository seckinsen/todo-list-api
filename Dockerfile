FROM golang:1.14.2 AS builder

ENV GO111MODULE=on
ENV GOARCH=amd64
ENV GOOS=linux
ENV GOPATH /go
ENV CGO_ENABLED=0
ENV PROJECT_NAME=todolist-api

WORKDIR ${GOPATH}/src/${PROJECT_NAME}

ADD . .

RUN go mod download
RUN go build -o main

FROM alpine:latest

RUN apk update && apk --no-cache add ca-certificates

ENV GOPATH /go
ENV PROJECT_NAME=todolist-api
ENV TIME_ZONE=Europe/Moscow
RUN ln -f -s /usr/share/zoneinfo/${TIME_ZONE}   /etc/localtime

WORKDIR /app

COPY --from=builder ${GOPATH}/src/${PROJECT_NAME}/main  main

RUN chmod +x main

EXPOSE 8000

ENTRYPOINT ["./main"]