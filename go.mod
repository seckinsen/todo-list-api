module todo-list-api

go 1.14

require (
	github.com/go-playground/validator/v10 v10.2.0
	github.com/google/uuid v1.1.1
	github.com/pact-foundation/pact-go v1.4.0
	github.com/stretchr/testify v1.5.1
)
