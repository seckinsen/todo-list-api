package main

import (
	"fmt"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/pact-foundation/pact-go/utils"
	"net/http"
	"os"
	"path/filepath"
	"testing"
	"todo-list-api/handler"
	"todo-list-api/repository"
)

var dir, _ = os.Getwd()
var logDir = fmt.Sprintf("%s/log", dir)
var pactDir = fmt.Sprintf("%s/pacts", dir)
var pactFileName = "todolist-ui-todolist-api.json"
var pactUrls = []string{filepath.FromSlash(fmt.Sprintf("%s/%s", pactDir, pactFileName))}
var port, _ = utils.GetFreePort()
var baseUrl = fmt.Sprintf("http://localhost:%d", port)
var brokerUrl = "https://test.pact.dius.com.au/"
var brokerUsername = "dXfltyFMgNOFZAxr8io9wJ37iUpY42M"
var brokerPassword = "O5AIZWxelWbLvqMd8PkAVycBJh2Psyg1"

var todoHandler *handler.Handler

var stateHandlers = types.StateHandlers{
	"I have todos list": func() error {
		todoHandler.Repository = repository.InitMock()
		return nil
	},
	"I want to add a todo to list": func() error {
		todoHandler.Repository = repository.Init()
		return nil
	},
	"I want to remove exist todo from list": func() error {
		todoHandler.Repository = repository.InitMock()
		return nil
	},
	"I want to remove not exist todo from list": func() error {
		todoHandler.Repository = repository.InitMock()
		return nil
	},
	"I want to revert exists todo status": func() error {
		todoHandler.Repository = repository.InitMock()
		return nil
	},
	"I want to revert not exist todo status": func() error {
		todoHandler.Repository = repository.InitMock()
		return nil
	},
	"I want to remove all todos from list": func() error {
		todoHandler.Repository = repository.InitMock()
		return nil
	},
}

func Test_TodoListApiProvider(t *testing.T) {
	commitHash := os.Getenv("COMMIT_HASH")
	if commitHash == "" {
		commitHash = "1.0.0"
	}

	startProvider()
	pact := createPact()

	// BeforeEach -> StateHandler -> RequestFilter (pre) -> Execute Provider Test -> RequestFilter (post) -> AfterEach
	resultLocal, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:    baseUrl,
		FailIfNoPactsFound: true,
		PactURLs:           pactUrls,
		ProviderVersion:    commitHash,
		StateHandlers:      stateHandlers,
	})

	if err != nil {
		fmt.Println(fmt.Sprintf("Error => %+v", err))
		t.Fatal()
	}
	fmt.Println(fmt.Sprintf("Result => %+v", resultLocal))

	result, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            baseUrl,
		BrokerURL:                  brokerUrl,
		BrokerUsername:             brokerUsername,
		BrokerPassword:             brokerPassword,
		PublishVerificationResults: true,
		ProviderVersion:            commitHash,
		StateHandlers:              stateHandlers,
	})

	if err != nil {
		fmt.Println(fmt.Sprintf("Error => %+v", err))
		t.Fatal()
	}
	fmt.Println(fmt.Sprintf("Result => %+v", result))
}

func startProvider() {
	// Start provider API in the background
	mux := http.NewServeMux()
	todoRepository := repository.Init()
	todoHandler = &handler.Handler{Repository: todoRepository}
	mux.HandleFunc("/todos", todoHandler.HandleTodos)
	mux.HandleFunc("/todos/", todoHandler.HandleTodo)
	go http.ListenAndServe(fmt.Sprintf(":%d", port), mux)
}

func createPact() dsl.Pact {
	// Create Pact connecting to local Daemon
	return dsl.Pact{
		Consumer:                 "todolist-ui",
		Provider:                 "todolist-api",
		LogDir:                   logDir,
		PactDir:                  pactDir,
		DisableToolValidityCheck: true,
		LogLevel:                 "DEBUG",
	}
}
