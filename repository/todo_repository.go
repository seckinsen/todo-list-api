package repository

import (
	"github.com/google/uuid"
	"sort"
	"todo-list-api/model"
)

type TodoRepository struct {
	todos map[string]model.Todo
}

func Init() *TodoRepository {
	return &TodoRepository{todos: make(map[string]model.Todo)}
}

func InitMock() *TodoRepository {
	return &TodoRepository{
		todos: map[string]model.Todo{
			"f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9": {
				Id:          "f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9",
				Description: "buy some milk",
				Completed:   false,
			},
			"982b62c4-119d-47f1-a249-586004f9d382": {
				Id:          "982b62c4-119d-47f1-a249-586004f9d382",
				Description: "rest for a while",
				Completed:   false,
			},
		},
	}
}

func (t *TodoRepository) GetTodos() []model.Todo {
	response := make([]model.Todo, 0)
	for _, todo := range t.todos {
		response = append(response, todo)
	}
	sort.Sort(model.ByDescription(response))
	return response
}

func (t *TodoRepository) AddTodo(request model.AddTodoRequest) string {
	id := uuid.New().String()
	t.todos[id] = model.Todo{
		Id:          id,
		Description: request.Description,
		Completed:   false,
	}
	return id
}

func (t *TodoRepository) DeleteTodos() {
	for key := range t.todos {
		delete(t.todos, key)
	}
}

func (t *TodoRepository) DeleteTodo(id string) bool {
	if _, ok := t.todos[id]; ok {
		delete(t.todos, id)
		return true
	}
	return false
}

func (t *TodoRepository) RevertTodoStatus(id string) bool {
	if _, ok := t.todos[id]; ok {
		todo := t.todos[id]
		todo.Completed = !todo.Completed
		t.todos[id] = todo
		return true
	}
	return false
}
