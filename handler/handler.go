package handler

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/go-playground/validator/v10/non-standard/validators"
	"github.com/google/uuid"
	"net/http"
	"strings"
	"todo-list-api/model"
	"todo-list-api/repository"
)

type Handler struct {
	Repository *repository.TodoRepository
}

func (h *Handler) HandleTodos(w http.ResponseWriter, req *http.Request) {
	activateCORS(w, req)
	todosHandleStrategies := h.getTodosHandleStrategies()
	if _, ok := todosHandleStrategies[req.Method]; !ok {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	todosHandleStrategies[req.Method](w, req)
}

func (h *Handler) HandleTodo(w http.ResponseWriter, req *http.Request) {
	activateCORS(w, req)
	todoHandleStrategies := h.getTodoHandleStrategies()
	if _, ok := todoHandleStrategies[req.Method]; !ok {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	todoHandleStrategies[req.Method](w, req)
}

func (h *Handler) getTodos(w http.ResponseWriter, req *http.Request) {
	todos := h.Repository.GetTodos()
	responseBytes, _ := json.Marshal(todos)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(responseBytes)
}

func (h *Handler) addTodo(w http.ResponseWriter, req *http.Request) {
	var request model.AddTodoRequest

	if err := json.NewDecoder(req.Body).Decode(&request); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	validate := validator.New()
	_ = validate.RegisterValidation("description", validators.NotBlank)
	if err := validate.Struct(request); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	id := h.Repository.AddTodo(request)
	response := model.AddTodoResponse{
		Id: id,
	}

	responseBytes, _ := json.Marshal(response)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(responseBytes)
}

func (h *Handler) deleteTodos(w http.ResponseWriter, req *http.Request) {
	h.Repository.DeleteTodos()
	w.WriteHeader(http.StatusNoContent)
}

func (h *Handler) deleteTodo(w http.ResponseWriter, req *http.Request) {
	id, valid := isValidTodoId(req.URL.Path)
	if !valid {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if !h.Repository.DeleteTodo(id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (h *Handler) revertTodoStatus(w http.ResponseWriter, req *http.Request) {
	id, valid := isValidTodoId(req.URL.Path)
	if !valid {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if !h.Repository.RevertTodoStatus(id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func isValidTodoId(path string) (string, bool) {
	id := strings.Replace(path, "/todos/", "", 1)
	_, err := uuid.Parse(id)

	if err != nil {
		return "", false
	}

	return id, true
}

func activateCORS(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
}

func (h *Handler) getTodosHandleStrategies() map[string]func(w http.ResponseWriter, req *http.Request) {
	todosHandleStrategies := make(map[string]func(w http.ResponseWriter, req *http.Request))

	todosHandleStrategies[http.MethodGet] = h.getTodos
	todosHandleStrategies[http.MethodPost] = h.addTodo
	todosHandleStrategies[http.MethodDelete] = h.deleteTodos
	todosHandleStrategies[http.MethodOptions] = func(w http.ResponseWriter, req *http.Request) {}
	return todosHandleStrategies
}

func (h *Handler) getTodoHandleStrategies() map[string]func(w http.ResponseWriter, req *http.Request) {
	todoHandleStrategies := make(map[string]func(w http.ResponseWriter, req *http.Request))

	todoHandleStrategies[http.MethodDelete] = h.deleteTodo
	todoHandleStrategies[http.MethodPatch] = h.revertTodoStatus
	todoHandleStrategies[http.MethodOptions] = func(w http.ResponseWriter, req *http.Request) {}

	return todoHandleStrategies
}
