package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"todo-list-api/model"
	"todo-list-api/repository"
)

func TestHandler_HandleTodos_GetTodosShouldSucceeded(t *testing.T) {
	handler := &Handler{Repository: repository.InitMock()}

	req := httptest.NewRequest(http.MethodGet, "/todos", nil)
	res := httptest.NewRecorder()

	handler.HandleTodos(res, req)

	var response []model.Todo
	_ = json.NewDecoder(res.Body).Decode(&response)

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, len(handler.Repository.GetTodos()), len(response))
}

func TestHandler_HandleTodos_AddTodoShouldNotSucceeded_WhenRequestIsNotValid(t *testing.T) {
	handler := &Handler{Repository: repository.InitMock()}

	request := model.AddTodoRequest{Description: "   "}
	requestByte, _ := json.Marshal(request)
	body := bytes.NewReader(requestByte)

	req := httptest.NewRequest(http.MethodPost, "/todos", body)
	res := httptest.NewRecorder()

	handler.HandleTodos(res, req)

	var response model.AddTodoResponse
	_ = json.NewDecoder(res.Body).Decode(&response)

	assert.Equal(t, http.StatusBadRequest, res.Code)
}

func TestHandler_HandleTodos_AddTodoShouldSucceeded(t *testing.T) {
	handler := &Handler{Repository: repository.InitMock()}

	request := model.AddTodoRequest{Description: "enjoy the assigment"}
	requestByte, _ := json.Marshal(request)
	body := bytes.NewReader(requestByte)

	req := httptest.NewRequest(http.MethodPost, "/todos", body)
	res := httptest.NewRecorder()

	handler.HandleTodos(res, req)

	var response model.AddTodoResponse
	_ = json.NewDecoder(res.Body).Decode(&response)

	assert.Equal(t, http.StatusCreated, res.Code)
	_, err := uuid.Parse(response.Id)
	assert.Nil(t, err)
}

func TestHandler_HandleTodos_DeleteTodosShouldSucceeded(t *testing.T) {
	handler := &Handler{Repository: repository.InitMock()}

	req := httptest.NewRequest(http.MethodDelete, "/todos", nil)
	res := httptest.NewRecorder()

	handler.HandleTodos(res, req)

	assert.Equal(t, http.StatusNoContent, res.Code)
}

func TestHandler_HandleTodos_NotSupportedOperationShouldReturnNotImplementedStatus(t *testing.T) {
	handler := &Handler{Repository: repository.Init()}

	req := httptest.NewRequest(http.MethodConnect, "/todos", nil)
	res := httptest.NewRecorder()

	handler.HandleTodos(res, req)

	assert.Equal(t, http.StatusNotImplemented, res.Code)
}

func TestHandler_HandleTodo_DeleteTodoShouldSucceeded(t *testing.T) {
	handler := &Handler{Repository: repository.InitMock()}

	id := handler.Repository.GetTodos()[0].Id

	req := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/todos/%s", id), nil)
	res := httptest.NewRecorder()

	handler.HandleTodo(res, req)

	assert.Equal(t, http.StatusNoContent, res.Code)
}

func TestHandler_HandleTodo_DeleteTodoShouldNotSucceeded_WhenTodoNotExist(t *testing.T) {
	handler := &Handler{Repository: repository.InitMock()}

	id := uuid.New().String()

	req := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/todos/%s", id), nil)
	res := httptest.NewRecorder()

	handler.HandleTodo(res, req)

	assert.Equal(t, http.StatusNotFound, res.Code)
}

func TestHandler_HandleTodo_RevertTodoStatusShouldSucceeded(t *testing.T) {
	handler := &Handler{Repository: repository.InitMock()}

	id := handler.Repository.GetTodos()[0].Id

	req := httptest.NewRequest(http.MethodPatch, fmt.Sprintf("/todos/%s", id), nil)
	res := httptest.NewRecorder()

	handler.HandleTodo(res, req)

	assert.Equal(t, http.StatusNoContent, res.Code)
}

func TestHandler_HandleTodo_RevertTodoStatusShouldNotSucceeded_WhenTodoNotExists(t *testing.T) {
	handler := &Handler{Repository: repository.InitMock()}

	id := uuid.New().String()

	req := httptest.NewRequest(http.MethodPatch, fmt.Sprintf("/todos/%s", id), nil)
	res := httptest.NewRecorder()

	handler.HandleTodo(res, req)

	assert.Equal(t, http.StatusNotFound, res.Code)
}

func TestHandler_HandleTodo_NotSupportedOperationShouldReturnNotImplementedStatus(t *testing.T) {
	handler := &Handler{Repository: repository.Init()}

	req := httptest.NewRequest(http.MethodConnect, "/todos", nil)
	res := httptest.NewRecorder()

	handler.HandleTodo(res, req)

	assert.Equal(t, http.StatusNotImplemented, res.Code)
}
