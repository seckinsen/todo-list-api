package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"todo-list-api/handler"
	"todo-list-api/repository"
)

func main() {
	todoRepository := repository.Init()
	todoHandler := &handler.Handler{Repository: todoRepository}
	http.HandleFunc("/todos", todoHandler.HandleTodos)
	http.HandleFunc("/todos/", todoHandler.HandleTodo)

	go func() {
		log.Println("Starting Server")
		if err := http.ListenAndServe(":8000", nil); err != nil {
			log.Fatalln(err)
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		s := <-signals
		log.Println("Shutting down")
		fmt.Println(s)
		time.Sleep(200 * time.Microsecond)
		cancel()
	}()

	<-ctx.Done()
}
